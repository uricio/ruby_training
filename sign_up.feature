Feature:
	As a user
	I want to be able to sign up to the system
	So that I can vallidate the sign up fields


	Scenario: Sign up a new user
		Given I am on landing page
		When I click on "My Account"
		And I click on "Sign Up"
		And I provide the firstname as "Test"
		And I provide the lastname as "Testname"
		And I provide the email as "test@email.com"	
		And I provide the password as "P4ssw0rd"
		And I provide the confirm password as "P@ssw0rd"
		And I click "sign up"
		Then I should be logged in to the application