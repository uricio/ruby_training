require_relative('../pages/home_page')
require_relative('../pages/sign_up_page')

Given(/^I launch HomePage$/) do
   $browser = Watir::Browser.new :chrome
   @home_page = HomePage.new($browser)

end

When(/^I click on "([^""]*)"$/) do |my_account|
  click_link(my_account)
end

And(/^I click on "([^""]*)"$/) do |sign_up|
	click_link(sign_up)
end
Then(/^I should be in sign_up_page$/) do
end


Given(/^I am in sign_up_page$/) do
end

When(/^I complete the firstname as "([^""]*)"$/) do |frstnm|
	fill_in 'firstname', :with => frstnm
end

And(/^I complete the lastname as "([^""]*)"$/) do |lstnm|
	fill_in 'lastname', :with => lstnm 
end

And(/^I complete the email_address as "([^""]*)"$/) do |email|
	fill_in 'email_address', :with => email
end

And(/^I complete the password as "([^""]*)"$/) do |pwd|
	fill_in 'password', :with => pwd	
end

And(/^I complete the confirm password as "([^""]*)"$/) do |confirmpwd|
	fill_in 'confirm password', :with => confirmpwd
end

And(/^I click sign-up$/) do
	Browser.button(:signup)

end

Then(/^I should be logged in to the application$/) do

end
