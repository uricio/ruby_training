class HomePage

  link(:my_account, :css => '#li_myaccount')
  link(:sign_up, :css => '#li_myaccount .go-text-right', index: 5)

  def initialize(browser)
    @browser = browser
   $browser.goto 'https://www.phptravels.net'
  end
end